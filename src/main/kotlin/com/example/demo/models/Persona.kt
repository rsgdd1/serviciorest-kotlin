package com.example.demo.models

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.GeneratedValue
import javax.persistence.OneToMany
import javax.persistence.Table
import javax.persistence.Column
import javax.persistence.GenerationType
import com.fasterxml.jackson.annotation.JsonBackReference
import org.hibernate.annotations.Cascade
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import javax.persistence.Basic

@Entity
@Table(name = "persona")
@ApiModel(description = "Todos los detalles sobre la persona")
class Persona(
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_persona")
	@ApiModelProperty(notes = "El ID de la persona generado por la BD")
	val idPersona: Int = 0,
	@Column
	@Basic(optional = false)
	@ApiModelProperty(notes = "El nombre de la persona")
	var nombre: String,
	@Column
	@Basic(optional = false)
	@ApiModelProperty(notes = "La edad de la persona")
	var edad: Int
) {
	private constructor() : this(0, "", 0)

	@OneToMany(mappedBy = "placaCarro")
	@Cascade({ CascadeType.SAVE_UPDATE })
	@JsonBackReference
	var carros: MutableSet<Carro> = HashSet()

}
