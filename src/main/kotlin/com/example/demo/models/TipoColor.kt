/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.demo.models

import com.fasterxml.jackson.annotation.JsonFormat

/*

  @author Raul A. Hernandez
  Created on Oct 5, 2019
*/

enum class TipoColor (val label: String){

	NEGRO("NEGRO"),
	AMARILLO("AMARILLO"),
	BLANCO("BLANCO"),
	ROJO("ROJO"),
	GRIS("GRIS"),
	VERDE("VERDE"),
	AZUL("AZUL");

}

