package com.example.demo.models

import com.fasterxml.jackson.annotation.JsonManagedReference
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import javax.persistence.*

@Entity
@Table(name = "carro")
@ApiModel(description = "Todos los detalles sobre el carro")
data class Carro(
	@Id
	@Column(name = "placa_carro")
	@ApiModelProperty(notes = "La placa del carro, siguiendo el formato ***-###")
	var placaCarro: String,
	@Column
	@ApiModelProperty(notes = "La marca del carro")
	var marca: String,
	@Column
	@ApiModelProperty(notes = "El modelo del carro")
	var modelo: String,
	@Column
	@Enumerated(EnumType.STRING)
	@ApiModelProperty(notes = "El color del carro")
	var color: TipoColor?,
	@JoinColumn(name = "id_persona", referencedColumnName = "id_persona")
	@ManyToOne(optional = false)
	@JsonManagedReference
	@ApiModelProperty(notes = "El propietario del carro")
	var persona: Persona?
) {
	private constructor() : this("", "", "", null, null)
}