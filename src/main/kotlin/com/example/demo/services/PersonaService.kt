package com.example.demo.services

import com.example.demo.models.Persona
import com.example.demo.repository.EntityService
import org.springframework.stereotype.Service
import com.example.demo.repositories.PersonaRepository
import org.springframework.beans.factory.annotation.Autowired
import com.example.demo.exceptions.NotFoundException

@Service
class PersonaService : EntityService<Persona, Int> {

	@Autowired
	lateinit var personaRepository: PersonaRepository

	override
	fun getAll(): ArrayList<Persona> {
		var lista: ArrayList<Persona> = ArrayList()
		personaRepository.findAll().forEach { t ->
			lista.add(t)
		}
		return lista
	}

	@Throws(NotFoundException::class)
	override
	fun getById(id: Int): Persona {
		return personaRepository.findById(id).orElseThrow {
			throw NotFoundException(id.toString())
		}
	}
	
	@Throws(NotFoundException::class)
	fun getByNombre(nombre: String): Persona {
		return personaRepository.findByNombre(nombre).orElseThrow {
			throw NotFoundException(nombre)
		}
	}

	override
	fun save(registry: Persona) {
		personaRepository.save(registry)
	}

	@Throws(NotFoundException::class)
	override
	fun modify(registry: Persona) {
		if (!exist(registry.idPersona)) {
			throw NotFoundException(registry.idPersona.toString())
		}
		personaRepository.save(registry)
	}

	override
	fun delete(id: Int) {
		personaRepository.deleteById(id)
	}

	override
	fun exist(id: Int): Boolean {
		return personaRepository.existsById(id)
	}

}