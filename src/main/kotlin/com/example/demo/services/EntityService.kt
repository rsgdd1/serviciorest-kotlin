package com.example.demo.repository

import java.util.ArrayList

interface EntityService<T, K> {

	fun getAll(): ArrayList<T>
	fun getById(id: K): T
	fun save(registry: T)
	fun modify(registry: T)
	fun delete(id: K)
	fun exist(id: K): Boolean

}