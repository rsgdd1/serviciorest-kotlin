package com.example.demo.services

import com.example.demo.repository.EntityService
import com.example.demo.models.Carro
import com.example.demo.repositories.CarroRepository
import org.springframework.beans.factory.annotation.Autowired
import java.util.ArrayList
import org.springframework.stereotype.Service
import kotlin.jvm.Throws
import java.lang.IllegalArgumentException

@Service
class CarroService : EntityService<Carro, String> {

	@Autowired
	lateinit var carroRepository: CarroRepository

	override
	fun getAll(): ArrayList<Carro> {
		var lista: ArrayList<Carro> = ArrayList()
		carroRepository.findAll().forEach { t ->
			lista.add(t)
		}
		return lista
	}

	@Throws(IllegalArgumentException::class)
	override
	fun getById(id: String): Carro {
		return carroRepository.findById(id).orElseThrow {
			throw IllegalArgumentException("No se encontró el elemento")
		}
	}
	
	@Throws(IllegalArgumentException::class)
	fun getByColor(color: String): Carro {
		return carroRepository.findByColor(color).orElseThrow {
			throw IllegalArgumentException("No se encontró el elemento")
		}
	}

	override
	fun save(registry: Carro) {
		carroRepository.save(registry)
	}


	@Throws(IllegalArgumentException::class)
	override
	fun modify(registry: Carro) {
		if (!exist(registry.placaCarro)) {
			throw IllegalArgumentException("El carro no existe en la BD")
		}
		carroRepository.save(registry)
	}

	override
	fun delete(id: String) {
		carroRepository.deleteById(id)
	}

	override
	fun exist(id: String): Boolean {
		return carroRepository.existsById(id)
	}
}