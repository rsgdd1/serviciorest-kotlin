package com.example.demo.exceptions

import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.http.HttpStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
class NotFoundException : RuntimeException {

	constructor(codigo: String?) : super("No encontrado el registro " + codigo);

}