package com.example.demo.repositories

import com.example.demo.models.Carro
import org.springframework.stereotype.Repository
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.Optional

@Repository
interface CarroRepository : JpaRepository<Carro, String> {

	@Query("SELECT c FROM Carro c where c.color = upper(?1)")
	fun findByColor(color: String): Optional<Carro>

}