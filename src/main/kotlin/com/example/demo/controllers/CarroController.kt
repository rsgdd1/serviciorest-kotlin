package com.example.demo.controllers

import com.example.demo.models.Carro
import com.example.demo.repository.EntityService
import org.springframework.beans.factory.annotation.Autowired
import com.example.demo.exceptions.NotFoundException
import com.example.demo.exceptions.ConflictException
import org.springframework.http.ResponseEntity
import java.net.URI
import org.springframework.http.HttpStatus
import com.example.demo.services.CarroService
import org.springframework.web.bind.annotation.*

@RequestMapping("/carro")
@RestController
@CrossOrigin("*")
class CarroController {

	@Autowired
	lateinit var carroService: CarroService

	@GetMapping
	fun getAll(): ResponseEntity<Any> {
		return ResponseEntity.ok(carroService.getAll())
	}

	@GetMapping("/{placa}")
	fun getByPlaca(@PathVariable placa: String): ResponseEntity<Any> {
		return ResponseEntity.ok(carroService.getById(placa))
	}

	@GetMapping("/color/{color}")
	fun getByColor(@PathVariable color: String): ResponseEntity<Any> {
		return ResponseEntity.ok(carroService.getByColor(color))
	}

	@PostMapping
	fun save(@RequestBody carro: Carro): ResponseEntity<Any> {
		if (carroService.exist(carro.placaCarro)) {
			throw ConflictException(carro.placaCarro.toString())
		}
		carroService.save(carro)
		return ResponseEntity
			.created(URI("/api/" + carro.placaCarro)).body("")
	}

	@Throws(NotFoundException::class, ConflictException::class)
	@PutMapping("/{placa}")
	fun modify(@PathVariable placa: String, @RequestBody carro: Carro): ResponseEntity<Any> {
		if (!carroService.exist(placa)) {
			throw NotFoundException(placa.toString())
		}
		if (!placa.equals(carro.placaCarro)) {
			throw ConflictException(placa.toString())
		}
		carroService.save(carro);
		return ResponseEntity.ok(URI("/api/" + carro.placaCarro))
	}

	@Throws(NotFoundException::class)
	@DeleteMapping("/{placa}")
	fun delete(@PathVariable placa: String): ResponseEntity<Any> {
		if (!carroService.exist(placa))
			throw NotFoundException(placa.toString())
		carroService.delete(placa);
		return ResponseEntity(HttpStatus.OK)
	}

}